# Service schema change Log

## [1.2] - 2018-7-13
  - ECSE-437: added classes: OperationMetadata, VariableAggregationMethods, SupportedInputProjections, SupportedOutputProjections, SupportedInputFormats, SupportedOutputFormats classes and MaxGranules field to ServiceOptions class. 
  - ECSE-431: added ESI and ECHO Orders enums. 
  - ECSE-433: Removed OnlineAccessURLPatternMatch and OnlineAccessURLPatternSubstitution fields.
  - removing deprecated SupportedProjection element
  - removed Uuid and NonAffiliated fields because they are not needed
  - Fixed UMM-S v1.2 schema issues identified in CMR integration.
  - Updated SupportedProjections and SupportedFormats enums based on issues identified in CMR migration.
  - Added missing Parameters field to UMM-S v1.2
  - Updated ProjectionName enum to include Spherical Mercator.

## [1.1] - 2018-5-2
  - Fixed comment typos
  - removed UUID sub element from ServiceOrganizationType
  - expanded the size of a few elements and sub elements to accommodate the SERF to UMM-S migration.
  - allowed for multiple RelatedURLs
  - Changed AccessConstraints and UseContraints to be single - not arrays or contain multiple values

## [1.0] - 2017-10-10
  - Initial version

Copyright © 2017 NASA
