# UMM Schema

The UMM schemas are defined using [JSON schema](http://json-schema.org/).

The UMM common schema defines fields that are present in multiple different UMM concept types. The UMM-C schema defines the UMM model for collections, The UMM-S schema defines the UMM model for services, UMM-Var schema defines the UMM model for variables and the UMM-G schema will defines the UMM model for granules.

UMM schemas are versioned starting from 1.0. The current version of the UMM schemas will always be the largest version number.

Use the following command to clone this git repo:

>git clone https://git.earthdata.nasa.gov/scm/emfd/unified-metadata-model.git

## DIF
The DIF 9 and DIF 10 are no longer maintained here. For those schemas please go to [dif-schemas.git](https://git.earthdata.nasa.gov/scm/emfd/dif-schemas.git).

>git clone https://git.earthdata.nasa.gov/scm/emfd/dif-schemas.git

## Versioning

Note that with the introduction of variable and service schemas, we moved to concept-type-level versioning, as development on each schema varied in the rate at which changes were being made. While the change in how we now do versioning reduces on-going overhead, it does mean a one-time change for consumers of the data.

In particular, after v1.9, concept-type schemas will be in the following directories in this repository:
* [collection](collection)
* [service][service]
* [variable][variable]

You will then find the latest version of each in their respecitve directories, with version numbers listed there.

Since variables and services are new, concept-type-level versioning for them has started at v1.0. Since previously this repo versioned only collections, the concept-type-level versioning for collections inherits the old version numbers.

Furthermore, we have copied all the old collection concept-type versioned schemas into the new location. As such, consumers of this repo can update their scripts to point to the new location for all old versions.
