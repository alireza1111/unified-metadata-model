# Granule schema change Log

## [1.5] - 2019-01-30
  - Added Track element for SWOT to HorizontalSpatialDomain as an optional element.
  - Added Track definition to include cycle, and passes. Passes is an array of a pass and an array of tiles.
  - Added a MetadataSpecification element section that contains single enumeration values so that all granules using UMM-G list the specification name, version, and the URL where the schema lives.
  - CMR-5688 Changed the length of Identifiers/Identifier from 80 to 128. This was added later.

## [1.4] - 2018-08-01
  - Initial version of this UMM-G that is being implemented.
  - List of changes while in development:
    - Incorporated description changes 
    - AccessConstraints made Value required
    - Change BeginOrbitNumber and EndOrbitNumber to be integers
    - AutomaticQualityFlag, OperationalQualityFlag, and ScienceQualityFlag made these enumeration types instead of free text.
    - putting the Granule_Mapping.xlsx file into the version 1.4 directory
    - adding units to the vertical spatial domains.  Fixing some mapping issues.
    - renamed to ProviderDates from MetadataProviderDates
    - added MimeType to FilePackageType and FileType
    - Added a range to the VirticalSpatialDomain
    - defined a checksum class that includes 
    - CMR-5062: Modified UMM-G schema to work around the limitation in CMR one-of/any-of handling code; Fixed UMM-G Sample data.
    - CMR-5062: Modified UMM-G schema to make UMM-G temporal EndingDateTime optional.
    - added Undetermined as a new enumeration value to AutomaticQualityFlag, OperationalQualityFlag and ScienceQualityFlag.
    - CMR-5212: Archive distribution change. Change ArchiveAndDistributionInformationType to avoid duplicate FileType.
  - CMR-5688 Changed the length of Identifiers/Identifier from 80 to 128. This was added later.

## [1.3 - 1.0] UMM-G versions that were delevered as deliverables on paper, but were never implemented. None of them included schemas.
Copyright © 2017 NASA
