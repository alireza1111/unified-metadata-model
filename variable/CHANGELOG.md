# Change Log

## [1.5] 2019-02-27
  - Added a main element of AcquisitionSourceName. The reason for this change is to have metadata records that can be uniquely identified by its contents. https://wiki.earthdata.nasa.gov/display/DUTRAIN/UMM-VAR+uniqueness
  - Rearranged UMM-Var so that the main schema is on top and the definitions for the main schema follow.
  - added maxItems and set it to 2 to constrain LatRange and LonRange to 2 points which make a bounding box
  - extended the length of Definition from 1024 to 10000.
 
## [1.4] 2019-01-29
  - Changed SizeEstimationType by removing the specific elements of AvgCompressionRateASCII and AvgCompressionRateNetCDF4 and replaced it with an element called AverageCompressionInfomration. This is an array which contains AverageCompressionInformationType(s) which contains a rate and an enumerated format element, both of which are required.

## [1.3] - 2018-10-19
  - ECSE-484: Added fields: Alias, SizeEstimation. 
              Added class: SizeEstimationType with fields: AverageCompressionSizeOfGranule, AvgCompressionRateASCII, AvgCompressionRateNetCDF4.
  - ECSE-484: Modified field length of Alias to 1024. Removed Alias field from SizeEstimation class. 
  - Changed the AverageCompressionSizeOfGranule to AverageSizeOfGranulesSampled and updating the sizeEstimationType ave compression rate descriptions.
  - Fixed the IndexRangesType LatRange and LongRange so that they both have a minimum of 2 points.

## [1.2] - 2018-07-06
  - ECSE-435: added fields: Characteristics/GroupPath, ValidRange/CodeSystemIdentifierMeaning, ValidRange/CodeSystemIdentifierValue, MeasurementIdentifiers, SamplingIdentifiers, VariableSubType. 
  - ECSE-432: added fields Dimensions/Type, Characteristics/Bounds/LowerLeft, Characteristics/Bounds/UpperRight.
  - ECSE-450: replaced fields: Replace Bounds by IndexRanges class.

## [1.1] - 2018-06-25
  - Removed a direct linking from variables to services by removing the top level Services element and all of its child elements. Made ScienceKeywords optional instead of required.

## [1.0] - 2017-10-10 Initial version

Copyright © 2017 NASA
