# Change Log

## [1.13] 2019-04-11 (Date from the CMR codebase of the last ticket implemented.)
  - CMR-4908 (CMR-5481 is the epic ticket) Added ArchiveAndDistributionInformation that replaced the old DistributionType and FileSizeType.

## [1.12] 2019-01-22 (Date from the CMR codebase)
  - CMR-5407 Added 4 new related URL keywords UMM-C and upgraded to version 1.12 (#679)

## [1.11] 2018-11-28
  - Changed the RelatedURL enumerations (Type and Subtype) so that they match the GCMD 8.6 version of the keywords that was released March 15 2018.

## [1.10] 2018-5-2
  - Changed UseConstraints to be an object that includes Description, LicenseURL, and LicenseText elements instead of holding a description string.
  - Replaced VerticalSystemDefinitionType with AltitudeSystemDefinitionType and DepthSystemDefinitionType.
  - Changed free text strings to enumerations for the following elements within AltitudeSystemDefinitionType and DepthSystemDefinitionType: AltitudeDistanceUnitsEnum, and DepthDistanceUnitsEnum
  - Removed EncodingMethod from both the AltitudeSystemDefinitionType and DepthSystemDefinitionType
  - GeographicCoordinateSystemType now has GeographicCoordinateUnits that is enumerated.
  - CollectionProgress is now enumerated.
  - Added the sub elements and the ability to explain that DOI's are not included on purpose. (MissingReason, Explanation)
  - Added MimeType sub element to Get Data for RelatedURLs
  - Added Format sub element to Get Service for RelatedURLs
  - Added MimeType sub element to OnlineResource and removed the required requirement for Name and Description so that only Linkage (a URL) is required.
  - Enumerated AdditionalAttributes DataType sub element
  - Removed TemporalRangeType sub element from TemporalExtentType
  - Enumerated VerticalSpatialDomainType
  - Enumerated TilingIdentificationSystemName

## [1.9] - 2017-3-21
  - Changed OnlineResource ApplicationProtocol to ApplicationProfile
  - Removed CenterPoint from Lines, GPolygons, and BoundingRectangles.
  - Moved DOI from CollectionCitation to a top level element
  - Added OnlineResourceType
  - In PublicationReferenceType and ResourceCitationType, changed RelatedUrl to OnlineResource
  - Changed SensorType to InstrumentChildType and added ComposedOf relationship to instruments
  - Added minItems to TilingIdenfiticationSystems in UMM-C so that the UMM tests work better
  - Changes to RelatedUrlType
    - Removed Title
    - Each RelatedUrl only contains one URL now, as opposed to an array of URLs
    - Replaced Relation with URLContentType, Type, and Subtype enumerations. URLContentType and Type are required.
    - Removed FileSize and MimeType.
    - Added GetService and GetData.
  - Removed RelatedUrls from the required list.
  - Added GetServiceType, GetDataType, and GetDataTypeFormatEnum.
  - Updated URLMimeTypeEnum to include Not provided.

## [1.8] - 2016-10-31
  - Added VersionDescription to collection
  - Made CollectionProgress required

## [1.7] - 2016-9-27
  - Changed ISOTopicCategory type from string to ISOTopicCategoryEnum.

## [1.6] - 2016-8-1
  - Changed ContactInformation to be a single instance instead of an array. This change affects
    the ContactInformations in DataCenter, ContactPerson, and ContactGroup.

## [1.5] - 2016-7-28
  - Changed AdditionalAttribute Description to be required

## [1.4]
  - Changed Organization to DataCenter

## [1.3] - 2016-5-12
  - Changed singular PaleoTemporalCoverage to plural PaleoTemporalCoverages

## [1.2] - 2016-5-10
  - SpatialKeywords is now deprecated, LocationKeywords will take its place.
  - ISOTopicCategories is now an uncontrolled vocabulary.
  - TilingCoordinateType's minimum value is not required.

## [1.1] - 2016-02-10
### UMM-C
  - Changed singular TilingIdentificationSystem to plural TilingIdentificationSystems

## [1.0] - Initial version

Copyright © 2016 NASA
